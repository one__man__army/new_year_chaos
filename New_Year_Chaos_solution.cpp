#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;


int main(){
    int T;
    cin >> T;
    for(int a0 = 0; a0 < T; a0++)
    {
        int n;
        cin >> n;
        vector<int> q(n);
        for(int q_i = 0; q_i < n; q_i++){
           cin >> q[q_i];
            }
    
        int temp_d = 0;
        int stop = 0;
        for (int i=n-1 ; i>=0 ; i--){
            int ind = i+1;
            int temp = 0;
            if ((q[i] - i) > 3){
                stop=1;
            }
            if (i != (n-1) && q[i+1] < q[i]){
                temp = q[i+1];
                q[i+1] = q[i];
                q[i] = temp;
                temp_d++;
                if (i != (n-2) && q[i+2] < q[i+1]){
                    temp = q[i+2];
                    q[i+2] = q[i+1];
                    q[i+1] = temp;
                    temp_d++;
                    }
            }
        }
        // Print answers
            if (!stop)
                cout << temp_d << endl;
            else    
                cout << "Too chaotic" << endl;

    }    
    return 0;
}