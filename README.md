# README #

Problem statement:
https://www.hackerrank.com/challenges/new-year-chaos/problem

### Solution report ###

* C++
* Uses one vector
* Space Complexity: O(n)
* Time Complexity: O(n^2)

### Libraries used ###

* map
* set
* list
* cmath
* ctime
* deque
* queue
* stack
* string
* bitset
* cstdio
* limits
* vector
* climits
* cstring
* cstdlib
* fstream
* numeric
* sstream
* iostream
* algorithm
* unordered_map

### Scoring ###

* Difficulty: Medium
* Max Score: 40
* Score: 40

### How to run ###
* Copy and paste solution into HackerRank 'current bufor', then submit. 